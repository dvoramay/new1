<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\Deal;
use app\models\Lead;
/* @var $this yii\web\View */
/* @var $model app\models\Lead */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Leads', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lead-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?php 
		if (\Yii::$app->user->can('updateLead') || 
		\Yii::$app->user->can('updateOwnLead', ['lead' =>$model]) ) { ?>
				<?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
		<?php }	?>	
        <?php if (\Yii::$app->user->can('createLead')) { ?>
		<?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
		<?php } ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'email:email',
            'phone',
            'notes:ntext',
			[ // the status name 
				'label' => $model->attributeLabels()['status'],
				'value' => $model->statusItem->name,	
			],			
			[ // the owner name of the lead
				'label' => $model->attributeLabels()['owner'],
				'format' => 'html',
				'value' => Html::a($model->userOwner->fullname, 
					['user/view', 'id' => $model->userOwner->id]),	
			],
			 
			[ // Lead created by
				'label' => $model->attributeLabels()['created_by'],
				'value' => isset($model->createdBy->fullname) ? $model->createdBy->fullname : 'No one!',	
			],
			[ // Lead created at
				'label' => $model->attributeLabels()['created_at'],
				'value' => date('d/m/Y H:i:s', $model->created_at)
			],
			[ // the deal of the lead
				'label' => $model->attributeLabels()['deals'],
				'format' => 'html',
				'value' => Html::a($model->dealsItem->name, 
					['deal/view', 'id' => $model->dealsItem->id]),	
			],
			
			[ // Lead updated by
				'label' => $model->attributeLabels()['updated_by'],
				'value' => isset($model->updateddBy->fullname) ? $model->updateddBy->fullname : 'No one!',	
			],
			[ // Lead updated at
				'label' => $model->attributeLabels()['updated_at'],
				'value' => date('d/m/Y H:i:s', $model->updated_at)
			],			
        ],
    ]) ?>

</div>
