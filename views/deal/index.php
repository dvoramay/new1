<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\DealSerach */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Deals';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="deal-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Deal', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            //'leadId',
			[
				'attribute' => 'leadId',
				'label' => 'lead Name',
				'format' => 'raw',
				'value' => function($model){
				return $model->dealItem->name;
				
				},
				'filter'=>Html::dropDownList('DealSearch[leadId]', $lead, $leads, ['class'=>'form-control']),
				],
            'name',
            'amont',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>