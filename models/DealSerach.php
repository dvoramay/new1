<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Deal;
use app\models\Lead;
////
/**
 * DealSerach represents the model behind the search form about `app\models\Deal`.
 */
class DealSerach extends Deal
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'leadId', 'amont'], 'integer'],
            [['name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Deal::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
		
		  // grid filtering conditions
	$this->leadId == -1? $this->leadId=null : $this->leadId;
      
        $query->andFilterWhere([
            'id' => $this->id,
            'leadId' => $this->leadId,
            //'amont' => $this->amont,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);
		$query->andFilterWhere(['like', 'leadId', $this->leadId]);
		$query->andFilterWhere(['>=', 'amont', $this->amont]);
        return $dataProvider;
	}
}