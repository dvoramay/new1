<?php

namespace app\models;
use Yii;
use yii\db\ActiveRecord;
use app\models\Lead;

/**
 * This is the model class for table "deal".
 *
 * @property integer $id
 * @property integer $leadId
 * @property string $name
 * @property integer $amont
 *
 * @property Lead $lead
 */
class Deal extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'deal';
    }

    /**
     * @inheritdoc
     */
   /**
     * @return \yii\db\ActiveQuery
     */
   
    public function rules()
    {
        return [
            [['leadId', 'name', 'amont'], 'required'],
            [['leadId', 'amont'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['leadId'], 'exist', 'skipOnError' => true, 'targetClass' => Lead::className(), 'targetAttribute' => ['leadId' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'leadId' => 'Lead ID',
            'name' => 'Name',
            'amont' => 'Amont',
        ];
    }
	 public static function getLeads()
	{
		$allLeads = self::find()->all();
		$allLeadsArray = ArrayHelper::
					map($allLeads, 'id', 'name');
		return $allLeadsArray;						
	}
	public function getLeadItem()
	{
      return $this->hasOne(Lead::className(), ['id' => 'leadId']);
	}
	 public function getLead()
     {
         return $this->hasOne(Lead::className(), ['id' => 'leadId']);
     }
	 
	public function getDealItem() 

  {
	   
	return $this->hasOne(Lead::className(), ['id' => 'leadId']);
    
	}
	
	public static function getDeals()  
	{		$allDeals = self::find()->all();
		    $allDealsArray = ArrayHelper::

					map($allDeals, 'id', 'name');
		return $allDealsArray;
	}
	
 }
    /**
     * @return \yii\db\ActiveQuery
     */
    

