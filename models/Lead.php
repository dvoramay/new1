<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
use yii\helpers\ArrayHelper;
use app\models\Deal;
/////
/**
 * This is the model class for table "lead".
 *
 * @property integer $id
 * @property string $name
 * @property string $email
 * @property string $phone
 * @property string $notes
 * @property integer $status
 * @property integer $owner
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 */
class Lead extends \yii\db\ActiveRecord
{
	public $deals;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'lead';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
		$rules = []; 
		$stringItems = [['notes'], 'string'];
		$integerItems  = ['status', 'created_at', 'updated_at', 'created_by', 'updated_by'];
		$dealItem = [['deals'], 'safe'];
		$dealItam = [['deals'], 'safe'];		
		if (\Yii::$app->user->can('updateLead')) {
			$integerItems[] = 'owner';
		}
		$integerRule = [];
		$integerRule[] = $integerItems;
		$integerRule[] = 'integer';
		$ShortStringItems = [['name', 'email', 'phone'], 'string', 'max' => 255]; 
		$rules[] = $stringItems;
		$rules[] = $integerRule;
		$rules[] = $ShortStringItems;		
		return $rules;
    }

    /**
     * @inheritdoc
     */

    /**
     * Defenition of relation to user table
     */    

	 public function behaviors()
    {
		return 
		[
			[
				'class' => BlameableBehavior::className(),
				'createdByAttribute' => 'created_by',
				'updatedByAttribute' => 'updated_by',
			 ],
				'timestamp' => [
				'class' => 'yii\behaviors\TimestampBehavior',
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
					ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
				],
			],
		];
    }
	public static function getLeads()
	{
		$allLeads = self::find()->all();
		$allLeadsArray = ArrayHelper::
					map($allLeads, 'id', 'name');
		return $allLeadsArray;						
	}
	public function getLeadItem()
	{
      return $this->hasOne(Lead::className(), ['id' => 'leadId']);
	}
	
    /**
     * Defenition of relation to user table
     */ 	
	
	public function getUserOwner()
    {
        return $this->hasOne(User::className(), ['id' => 'owner']);
    }
	
	public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }	

	public function getUpdateddBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }	
	
    /**
     * Defenition of relation to status table
     */  
 	
 
	public function getStatusItem()
    {
        return $this->hasOne(Status::className(), ['id' => 'status']);
    }


	
	public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'email' => 'Email',
            'phone' => 'Phone',
            'notes' => 'Notes',
            'status' => 'Status',
            'owner' => 'Owner',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
			'deals' => 'Deals',
        ];
    }
	public function getDeals()

   {
       return $this->hasMany(Deal::className(), ['leadId' => 'id']);
	}
	//q4c
	public static function getLeadsWithAllLeads()
	{
		$allLead = self:: getLeads();
		$allLead[-1] = 'All Leads';
		$allLead = array_reverse ( $allLead, true );
		return $allLead;	
	}
	//q4c
	public static function getExistLeadsWithAllLeads()
	{
		//get all the leads id in the deal table
		$deals = new Deal();
		$dealsExist= $deals->find()->select('leadId')->distinct()->all();
		$dealsExistArr = [];
		foreach ($dealsExist as $val) {
			$dealsExistArr[] = $val->leadId;
		}
		
		//select only the lead that exist in deal table
		$allLeads = self::find()->where(['id'=>$dealsExistArr])->all();
		$allLeadsArray = ArrayHelper::
					map($allLeads, 'id', 'name');
	
		$allLeadsArray[-1] = 'All Leads';
		$allLeadsArray = array_reverse ( $allLeadsArray, true );
		return $allLeadsArray;	
	}
	//q5a
	public function getDealsItem()

    {
        return $this->hasOne(Deal::className(), ['leadId' => 'id']);
    }
}